package edu.ecpi.benboy4773.marcopolo;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;

public class MarcoPolo {

    private static int getSpeakerCount(){
    	System.out.print("How many Speakers in your game? ");
        
    	Scanner in = new Scanner(System.in);
    	//TODO: Validate input is actually a number
        int num = in.nextInt();
        in.close();
        
    	return(num);
    }
	
	public static void main(String[] args) {
		//Get number of speakers
		int speakerCount = getSpeakerCount();
		
		//Create and initialize array
		Speaker[] speakerList = new Speaker[speakerCount]; //init to the number of speakers we have
		HashSet bag = new HashSet<Integer>(); //Integer objects rather than primitives
		
		for (int i=0; i < speakerCount; i++){
			  speakerList[i] = new Speaker("Speaker " + Integer.toString(i+1) );
			  bag.add(i);
		}
		
		System.out.println("bag: " + bag);
		
		Iterator<Integer> bagIterator = bag.iterator();
				
		int firstSpeaker = bagIterator.next();
		int currentSpeaker = firstSpeaker;
		int nextSpeaker;

		while (bagIterator.hasNext()) {
			nextSpeaker = bagIterator.next();
			speakerList[currentSpeaker].setNextSpeaker(speakerList[nextSpeaker]);
			currentSpeaker = nextSpeaker;
		}
		
		//Write 'em out so we can see them
		for (int i=0; i < speakerCount; i++){
			  System.out.println(speakerList[i].toString() );
		}
			
		//Start run
		speakerList[firstSpeaker].say("Marco!");
	}

}
