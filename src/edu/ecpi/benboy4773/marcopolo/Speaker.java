package edu.ecpi.benboy4773.marcopolo;

public class Speaker {
	protected String name;
	protected Speaker nextSpeaker;
	
	public void setName(String name){
		this.name = name;
	}
	public String getName(){
		return this.name;
	}
	public void setNextSpeaker(Speaker nextSpeaker){
		this.nextSpeaker = nextSpeaker;
	}
	public Speaker getNextSpeaker(){
		return this.nextSpeaker;
	}

	
	//Constructors
	public Speaker (){
		this("John Doe");
	}
	
	public Speaker (String name){
		this(name, null);
	}
	
	public Speaker(String name, Speaker nextSpeaker){
		this.setName(name);
		this.setNextSpeaker(nextSpeaker);
	}
	
	//Methods
	
	public void say(String message){
		System.out.println(this.getName() + ": " + message);
		
		if ( message.equalsIgnoreCase("Marco!") ){
			message = "Polo!";
		}
		else message = "Marco!";

		if ( this.getNextSpeaker() != null ){
			this.getNextSpeaker().say(message);
		}
		else {
			//BJB - We don't want the speaker to say anything else
			//System.out.println(this.getName() + ": I'm lonely!");
		}
	}
	
	public String toString(){
		String intro;
		intro = "My name is " + this.getName();
		
		if ( this.getNextSpeaker() != null ){
			intro += ", and my listener is " + this.getNextSpeaker().getName();
		}
		
		return intro;
	}
}
